<?php
/**
 * Important : Do not remove $page['content']
 */
 ?>
 
<div class="main-container container">
	<div class="center-block">
		<div class="row">
			<div class="col-md-4"></div>
			<div class="col-md-4">
				<?php print render($page['content']); ?>
			</div>
			<div class="col-md-4"></div>
		</div>
	</div>
</div>

